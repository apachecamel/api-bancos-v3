package com.accenture.spring.camel.api.processor.log;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.dto.Resultados;
import com.accenture.spring.camel.api.model.Cuenta;

@Component
public class LogearFallidos implements Processor {
	
	@Autowired
	private Resultados resultados;

	@Override
	public void process(Exchange exchange) throws Exception {
		
		resultados.getFallidos().add(exchange.getIn().getBody(Cuenta.class));

		
	}

}
