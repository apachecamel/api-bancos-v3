package com.accenture.spring.camel.api.resource.rest;

import java.util.Date;

import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangeTimedOutException;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.transaction.CannotCreateTransactionException;

import com.accenture.spring.camel.api.constants.ErrorConstants;
import com.accenture.spring.camel.api.constants.URI;
import com.accenture.spring.camel.api.dto.CuentaList;
import com.accenture.spring.camel.api.model.Cuenta;
import com.accenture.spring.camel.api.model.ErrorMessage;
import com.accenture.spring.camel.api.processor.BancoProcessor;
import com.accenture.spring.camel.api.processor.MultipleProcessor;
import com.accenture.spring.camel.api.processor.log.LogearSingleton;
import com.accenture.spring.camel.api.resource.ErrorHandlerRoute;

/**
 * Contiene todas las rutas REST de tipo GET.
 * 
 * @Authors ignacio.speicys , santiago.ferreiro
 *
 */
@Component
public class PostResource extends ErrorHandlerRoute {

	@BeanInject
	private MultipleProcessor procesadormultiple;

	@BeanInject
	private LogearSingleton logeador;
	
	@BeanInject
	private BancoProcessor bancoprocesador;

	@Override
	public void configure() throws Exception {
		super.configure();
		
		rest("/cuenta")
		
		/**
		 * 	tener en cuenta que "/single" ; "/activemq" ; "/many"
		 * 	son solo para tests de componentes camel, 
		 * 	mirar el ejemplo integrador mas abajo..
		 */
			.post("/single")
				.consumes(MediaType.APPLICATION_JSON_VALUE)
				.type(Cuenta.class)
				.description("Proposito de Testing")
				.route()
					.doTry()
						.to("seda:bddtest?timeout=2000")
						.doCatch(ExchangeTimedOutException.class)
							.setBody(()-> new ErrorMessage(ErrorConstants.BASE_CAIDA))
							.setHeader(Exchange.HTTP_RESPONSE_CODE, simple("406"))
							.to("direct:marshalError")
						.endDoTry()
						.doCatch(CannotCreateTransactionException.class)
							.setBody(()-> new ErrorMessage(ErrorConstants.CATCH_ALL))
							.setHeader(Exchange.HTTP_RESPONSE_CODE, simple("500"))
							.to("direct:marshalError")
						.endDoTry()
			.endRest()
	
			.post("/activemq")
				.consumes(MediaType.APPLICATION_JSON_VALUE)
				.type(Cuenta.class).outType(Cuenta.class)
				.description("Proposito de Testing")
				.route()
					.marshal().json(JsonLibrary.Jackson, Cuenta.class)
					.wireTap("activemq:queue:banco2")
					.unmarshal().json(JsonLibrary.Jackson, Cuenta.class)
			.endRest()
	
			.post("/many")
				.consumes(MediaType.APPLICATION_JSON_VALUE)
				.type(CuentaList.class).outType(CuentaList.class)
				.description("Deprecated")
				.route()
					.process(procesadormultiple)
			.endRest()
	
			.post()
				.produces(MediaType.APPLICATION_JSON_VALUE).consumes(MediaType.APPLICATION_JSON_VALUE)
				.type(Cuenta[].class)
				//cambiarlo 
				
				// SWAGGER
				.description("Post para insertar n cuentas a la BDD")
				.responseMessage().code(200).message("OK")
				.responseModel(Cuenta.class).endResponseMessage()
				.responseMessage().code(400).message("BDD OFFLINE")
				.responseModel(ErrorMessage.class).endResponseMessage()
				
				// SIGUE LA RUTA
				.route()
					.doTry()
						.routeId("PostCuentas")
						.split().body().parallelProcessing()
							
							.choice()
								.when().simple("${body.categoria} == 1").to("seda:categoria")
								.when().simple("${body.categoria} > 1").to("direct:prebdd")
								.otherwise()
									.to("direct:fail").marshal().json(JsonLibrary.Jackson, Cuenta.class)
									.multicast().parallelProcessing()
									.to("file:logs/json/" + new Date().getTime(),
											"file:logs/json?fileName=unijson&fileExist=Append")
								.endChoice()	
							.end()
						.end()
					.process(logeador)
					.endDoTry()
					.doCatch(ExchangeTimedOutException.class).to(URI.Direct.BDD.FAIL)
					.endDoTry()
			.endRest();
	}

}
