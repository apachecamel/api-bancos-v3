package com.accenture.spring.camel.api.processor.id;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.json.simple.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.dao.CuentaDao;
import com.accenture.spring.camel.api.exceptions.CuentaNotFoundException;

@Component
public class BorrarCuentaByIdProcessor implements Processor {

	@Autowired
	private CuentaDao cuentadao;

	@Override
	public void process(Exchange exchange) throws Exception {
		JsonObject j = new JsonObject();

			if (cuentadao.findById(Long.parseLong(exchange.getMessage().getHeader("id").toString()))
					.orElse(null) == null) {

				throw (new CuentaNotFoundException());

			} else {
				j.put("Cuenta Eliminada, (ID)", Long.parseLong(exchange.getMessage().getHeader("id").toString()));
				// intenta borrar el banco solo si este existe en la bdd
				cuentadao.deleteById(Long.parseLong((String) exchange.getMessage().getHeader("id")));
				j.put("error", 0);
				exchange.getOut().setBody(j);
			}

	}

}
