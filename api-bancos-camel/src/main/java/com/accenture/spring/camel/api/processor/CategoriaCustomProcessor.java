package com.accenture.spring.camel.api.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.json.simple.JsonArray;
import org.apache.camel.json.simple.JsonObject;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.model.Cuenta;

@Component
public class CategoriaCustomProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		
		JsonObject obj = new JsonObject();
		
		JsonArray array = new JsonArray();
		
		Cuenta aux = exchange.getIn().getBody(Cuenta.class);
		String categoria;
		
		if (aux.getCategoria() == 1) 
			categoria = " (STANDARD)";
		 else if (aux.getCategoria() == 2) 
			categoria = " (GOLD)";
		 else 
			categoria = " (BLACK)";
		
		aux.setName(aux.getName() + categoria);
		
		obj.put("id", aux.getId());
		obj.put("name", aux.getName());
		
		array.add(obj);
		
		//no hace nada, falta bean para poder hacer referencia a la lista desde otras partes del programa
		exchange.getOut().setBody(array);
		
		
	}

}
