package com.accenture.spring.camel.api.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.model.Cuenta;
import com.accenture.spring.camel.api.service.BancoService;

@Component
public class CategoriaProcessor implements Processor {
	
	@Autowired
	private BancoService inMemoryBank;

	@Override
	public void process(Exchange exchange) throws Exception {
		
		//averiguar que hay en el header del exchange
		inMemoryBank.addCuenta((Cuenta)exchange.getIn().getBody());
	}
}
