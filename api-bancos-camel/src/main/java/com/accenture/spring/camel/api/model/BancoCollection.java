package com.accenture.spring.camel.api.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "bancos")
@XmlAccessorType(XmlAccessType.FIELD)
public class BancoCollection {

	@XmlElement(name = "banco")
	private List<BancoModel> bancos;
	
	public BancoCollection() {
		
		bancos = new ArrayList<BancoModel>();
	}
	
	public List<BancoModel> getBancos() {
		return bancos;
	}
	
	public void agregarBanco(BancoModel banco) {
		
		bancos.add(banco);
	}
}
