package com.accenture.spring.camel.api.resource;

import org.apache.camel.builder.RouteBuilder;

import com.accenture.spring.camel.api.constants.URI;

/**
 * Routebuilder generico para catchear errores que no estan cubiertos en trycatch particulares.
 * 
 * @author santiago.ferreiro
 *
 */
public abstract class ErrorHandlerRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		
		onException(Exception.class).log("Error Generico Disparado")
		.to(URI.Mock.CATCH_ALL);
	}

}
