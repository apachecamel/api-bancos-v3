package com.accenture.spring.camel.api.exceptions;

public class CuentaNotFoundException extends AppException {
	
	@Override
	public String getMensaje() {
		
		return "No existe una cuenta con ese ID";
	}
	
	
	
	
}
