package com.accenture.spring.camel.api.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accenture.spring.camel.api.model.BancoModel;

public interface BancoDao extends JpaRepository<BancoModel, Long> {
	
	public BancoModel findByName(String name);

}
