package com.accenture.spring.camel.api.model;

public class ErrorMessage {
	
	private String message;
	private int error;
	
	public int getError() {
		return error;
	}
	
	public void setError(int error) {
		this.error = error;
	}

	public ErrorMessage(String message) {
		
		this.message = message;
		this.error = 1;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
	
}
