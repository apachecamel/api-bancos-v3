package com.accenture.spring.camel.api.exceptions;

public abstract class AppException extends Exception {

	private String mensaje = "Mensaje generico de error";

	public void mostrarMensaje() {

		System.out.println("Error! Error! Error!");
	}

	public String getMensaje() {
		return mensaje;
	}

}
