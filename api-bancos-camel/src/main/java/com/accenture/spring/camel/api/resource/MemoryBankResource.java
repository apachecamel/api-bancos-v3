package com.accenture.spring.camel.api.resource;

import org.apache.camel.BeanInject;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.model.Cuenta;
import com.accenture.spring.camel.api.model.ErrorMessage;
import com.accenture.spring.camel.api.processor.BDDProcessor;
import com.accenture.spring.camel.api.processor.RemoveBankProcessor;
import com.accenture.spring.camel.api.service.BancoService;

@Component
public class MemoryBankResource extends ErrorHandlerRoute {

	@Autowired
	private BancoService inMemoryBank;

	@Autowired
	private RemoveBankProcessor removerbanco;

	@BeanInject
	private BDDProcessor processorBDD;

	@Override
	public void configure() throws Exception {
		super.configure();
		

		rest("/memorybank").get().produces(MediaType.APPLICATION_JSON_VALUE)
		 //SWAGGER
		.description("Devuelve la lista en MemoryBank. Deprecated")
//		 .param().name("id").type(RestParamType.path).description("Id de la cuenta").required(true).dataType("string").endParam()
		 .responseMessage().code(200).message("OK").responseModel(Cuenta.class).endResponseMessage() //OK
		 //RUTA
		.route()
		.setBody(() -> inMemoryBank.getInMemoryBank()).endRest()

		.post().produces(MediaType.APPLICATION_JSON_VALUE)
		 //SWAGGER
		.description("Agrega el MemoryBank a la Base de datos. Deprecated")
//		 .param().name("id").type(RestParamType.path).description("Id de la cuenta").required(true).dataType("string").endParam()
		 .responseMessage().code(200).responseModel(Cuenta.class).endResponseMessage() //OK
		 .responseMessage().code(400).responseModel(ErrorMessage.class).endResponseMessage() //Not-OK
		 //RUTA

		.route().setBody(() -> inMemoryBank.getInMemoryBank())
				.split().simple("body").process(processorBDD).process(removerbanco).endRest();

	}

}
