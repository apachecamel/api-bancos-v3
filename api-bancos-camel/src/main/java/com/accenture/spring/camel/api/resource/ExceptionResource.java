package com.accenture.spring.camel.api.resource;

import org.apache.camel.Exchange;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.constants.ErrorConstants;
import com.accenture.spring.camel.api.constants.URI;
import com.accenture.spring.camel.api.model.ErrorMessage;

@Component
public class ExceptionResource extends ErrorHandlerRoute {
	

	@Override
	public void configure() throws Exception {
		super.configure();
		
		from(URI.Direct.Marshall.ERROR)
			.marshal().json(JsonLibrary.Jackson, ErrorMessage.class);
		
		//RUTAS EXCEPCIONES
		
		from(URI.Direct.Error.CUENTA)
			.setBody(()-> new ErrorMessage(ErrorConstants.CUENTA_NOT_FOUND))
			.setHeader(Exchange.HTTP_RESPONSE_CODE, simple("404"))
			.to(URI.Direct.Marshall.ERROR);
	
		from(URI.Direct.BDD.FAIL)
			.setBody(()-> new ErrorMessage(ErrorConstants.BASE_CAIDA))
			.setHeader(Exchange.HTTP_RESPONSE_CODE, simple("500"))
			.to(URI.Direct.Marshall.ERROR);
		
		from(URI.Direct.Error.TRANSACCION)
			.setBody(()-> new ErrorMessage(ErrorConstants.TRANSACCION_INVALIDA))
			.setHeader(Exchange.HTTP_RESPONSE_CODE, simple("406"))
			.to(URI.Direct.Marshall.ERROR);
		
		from(URI.Direct.Error.SALDO_INSUFICIENTE)
			.setBody(()-> new ErrorMessage(ErrorConstants.SALDO_INSUFICIENTE))
			.setHeader(Exchange.HTTP_RESPONSE_CODE, simple("406"))
			.to(URI.Direct.Marshall.ERROR);

		from("direct:catchAll")
			.setBody(()-> new ErrorMessage(ErrorConstants.CATCH_ALL))
			.setHeader(Exchange.HTTP_RESPONSE_CODE, simple("500"))
			.to(URI.Direct.Marshall.ERROR);
		
		
	}

}
