package com.accenture.spring.camel.api.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.dao.BancoDao;
import com.accenture.spring.camel.api.model.BancoModel;

@Component
public class BancoProcessor implements Processor {
	
	@Autowired
	private BancoDao bancodao;

	@Override
	public void process(Exchange exchange) throws Exception {
		
		//creo un banco para agarrar el exchange
		BancoModel banco = exchange.getIn().getBody(BancoModel.class);
		
		//creo un banco auxiliar que es igual al de la base de datos con el nombre de arriba
		
		if (bancodao.findByName(banco.getName()) != null) {
			
			BancoModel aux = bancodao.findByName(banco.getName());
			
			aux.setClientes(aux.getClientes() + banco.getClientes());
			
			aux.setRecursos(aux.getRecursos() + banco.getRecursos());
			
			bancodao.save(aux);
				
			} else {
			
			//si no existe en la tabla un banco con ese nombre, creo uno nuevo
			
			bancodao.save(exchange.getIn().getBody(BancoModel.class));
				
			}
		
	}

}
