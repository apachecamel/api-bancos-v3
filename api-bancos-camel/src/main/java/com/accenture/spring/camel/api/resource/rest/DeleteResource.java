package com.accenture.spring.camel.api.resource.rest;

import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.apache.camel.model.rest.RestParamType;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.constants.ErrorConstants;
import com.accenture.spring.camel.api.exceptions.CuentaNotFoundException;
import com.accenture.spring.camel.api.model.Cuenta;
import com.accenture.spring.camel.api.model.ErrorMessage;
import com.accenture.spring.camel.api.processor.id.BorrarCuentaByIdProcessor;
import com.accenture.spring.camel.api.resource.ErrorHandlerRoute;
/**
 * Contiene todas las rutas REST de tipo DELETE.
 * 
 * @Authors ignacio.speicys, santiago.ferreiro
 *
 */
@Component
public class DeleteResource extends ErrorHandlerRoute {

	@BeanInject
	private BorrarCuentaByIdProcessor borradorid;

	@Override
	public void configure() throws Exception {
		super.configure();
		rest("/cuenta")
			.delete("/{id}")
				.produces(MediaType.APPLICATION_JSON_VALUE)
				// SWAGGER
				.description("Borra la cuenta por el ID definido.")
					.param()
						.name("id")
						.type(RestParamType.path)
						.description("Id de la cuenta")
						.required(true)
						.dataType("string")
					.endParam()
					.responseMessage()
						.code(200)
						.message("OK")
						.responseModel(Cuenta.class)
					.endResponseMessage() // OK
					.responseMessage()
						.code(404)
						.message("CUENTA NOT FOUND")
						.responseModel(ErrorMessage.class)
					.endResponseMessage() // Not-OK
				// RUTA
				.route()
					.doTry()
						.process(borradorid)
					.endDoTry()
					.doCatch(CuentaNotFoundException.class)
				.to("direct:cuentaNotFound")
				.endRest();

	}

}
