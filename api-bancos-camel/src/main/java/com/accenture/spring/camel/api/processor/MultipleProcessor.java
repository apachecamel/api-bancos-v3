package com.accenture.spring.camel.api.processor;

import java.util.Arrays;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.json.simple.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.dao.CuentaDao;
import com.accenture.spring.camel.api.dto.CuentaList;
import com.accenture.spring.camel.api.model.Cuenta;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class MultipleProcessor implements Processor {
	
	@Autowired
	private CuentaDao cuentadao;
	
	private CuentaList cuentalist;

	@Override
	public void process(Exchange exchange) throws Exception {
		
		cuentalist = exchange.getIn().getBody(CuentaList.class);
		
		System.out.println("tamaño de la lista:"+cuentalist.getListaCuentas().size());
		
		for (int i=0 ; i< cuentalist.getListaCuentas().size() ; i++) {
			
			cuentadao.save(cuentalist.getListaCuentas().get(i));
			
		} 
		
		
		
	}

}
