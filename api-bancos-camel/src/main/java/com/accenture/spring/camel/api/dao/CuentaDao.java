package com.accenture.spring.camel.api.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.accenture.spring.camel.api.model.Cuenta;

public interface CuentaDao extends JpaRepository<Cuenta, Long> {

}
