package com.accenture.spring.camel.api.processor.id;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.json.simple.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.dao.CuentaDao;
import com.accenture.spring.camel.api.exceptions.CuentaNotFoundException;
import com.accenture.spring.camel.api.exceptions.SaldoInsuficienteException;
import com.accenture.spring.camel.api.model.Cuenta;

@Component
public class ExtraccionProcessor implements Processor {

	@Autowired
	private CuentaDao cuentadao;

	@Override
	public void process(Exchange exchange) throws Exception {
		JsonObject j = new JsonObject();
		// creo una cuenta que alvergue los pocos datos que vienen (id + extraccion)
		Cuenta extraccion = exchange.getIn().getBody(Cuenta.class);

		// creo una cuenta auxiliar y le asigno la cuenta guardada por id en bdd
		Cuenta banco1 = cuentadao.findById(extraccion.getId()).orElse(null);

			if (banco1 == null) {
				throw (new CuentaNotFoundException());
			} else if ((extraccion.getSaldo() * -1) > banco1.getSaldo()) {

				// si se quiere extraer mas de lo que la cuenta posee, arroja la excepcion de
				// saldo insuficiente.
				throw (new SaldoInsuficienteException());

			} else {

				// sustraigo la extraccion del saldo actual, no toco id
				banco1.setSaldo(banco1.getSaldo() + extraccion.getSaldo());

				// finalmente guardo la cuenta modificada en bdd.
				cuentadao.save(banco1);

				exchange.getOut().setBody(banco1);
			}

	}

}
