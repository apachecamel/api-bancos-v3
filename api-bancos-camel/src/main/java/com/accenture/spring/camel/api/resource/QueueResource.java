package com.accenture.spring.camel.api.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.component.jackson.ListJacksonDataFormat;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.constants.URI;
import com.accenture.spring.camel.api.model.BancoModel;
import com.accenture.spring.camel.api.model.Cuenta;
import com.accenture.spring.camel.api.processor.ArchivoProcessor;
import com.accenture.spring.camel.api.resource.strategy.BancoAggregationStrategy;

/**
 * 
 * Contiene todas las rutas que consumen desde colas/topics ActiveMQ
 * 
 * @author ignacio.speicys
 *
 */
@Component
public class QueueResource extends ErrorHandlerRoute {
	
	@BeanInject
	private ArchivoProcessor procesarArchivo;

	@Override
	public void configure() throws Exception {
		super.configure();
		
		from(URI.Queue.BANK)
			.unmarshal().json(JsonLibrary.Jackson, Cuenta.class)
			.to(URI.Direct.BDD.IN);
		
		from(URI.Queue.BACKUP)
			.log("llega desde queue:backup a /file")
			.to("file:fail");
		/**
		 *  Permite distribuir la carga entre varios endpoints para procesar la informacion de manera mas rapida
		 */
		
		from(URI.Queue.LOAD_BALANCER)
			.routeId("AdaptadorDeQ").log("Llego al Adaptador")
				.unmarshal().json(JsonLibrary.Jackson, Cuenta[].class)
				.split().body()
					.multicast().parallelProcessing()
						.to("vm:loaderOne","vm:loaderTwo");
		
		from("vm:loaderOne")
			.routeId("LoadBalancer_1").log("llego al: ${routeId}")
				.loadBalance()
					.roundRobin().to("seda:saveToFile1", "seda:saveToFile2", "seda:saveToFile3")
			.end();
		
		from("vm:loaderTwo")
			.routeId("LoadBalancer_2").log("llego al: ${routeId}")
				.loadBalance()
					.weighted(false, "1,4,2") 
						.to("seda:savetoFile1", "seda:saveToFile2", "seda:saveToFile3")
			.end();	
		/**
		 *  VM es una extension de SEDA, provee funcionalidad para la comunicacion entre 2 instancias distintas
		 *  de Camel Context, en este caso no hace nada distinto al SEDA.
		 */
		
		from("seda:saveToFile1").routeId("STF1").delay(1000).log("llego a: ${routeId}");
		from("seda:saveToFile2").routeId("STF2").delay(1000).log("llego a: ${routeId}");
		from("seda:saveToFile3").routeId("STF3").delay(1000).log("llego a: ${routeId}");
		
		
		from(URI.File.DISPARADOR).log("${body}")
			.to("activemq:topic:VirtualTopic.nacho");  //sintaxis para pegarle al VirtualTopic
		/**
		 *  muestra los nombres de los bancos que vienen mediante logs
		 */
		
		from("activemq:queue:Consumer.1.VirtualTopic.nacho").routeId("consumer1").log("consumiendo.. ${routeId}")
			.unmarshal().json(JsonLibrary.Jackson, BancoModel[].class)
			.split().body()
				.log("Nombre: ${body.name} , Clientes: ${body.clientes}")
			.end()
			.log("..Finaliza Consumer 1..");
		/**
		 *  hace un resequence en batch y muestra ordenado de mayor a menor recursos totales
		 */
		from("activemq:queue:Consumer.2.VirtualTopic.nacho").routeId("consumer2").log("consumiendo.. ${routeId}")
			.unmarshal().json(JsonLibrary.Jackson, BancoModel[].class)
			.split().body()
				.resequence(simple("${body.recursos}")).batch().timeout(5*1000)
				.log("Nombre: ${body.name}, Recursos: ${body.recursos}")
				.end()
			.end()	
		.log("..Bancos Ordenados por recursos..");
		/**
		 *  recibe el json entero y lo rutea a posibles endpoints con distintos intervalos de frequencia
		 */
		from("activemq:queue:Consumer.3.VirtualTopic.nacho").routeId("consumer3").log("llego al: ${routeId}")
				.loadBalance()
					.weighted(false, "1,4,2") 
						.to("mock:guardarEnBLOB", "mock:guardarEnXML", "mock:guardarEnJSON")
				.end();
		/**
		 *  se va a Appendear un XML 4 veces mas que en BLOB y 2 veces mas que en JSON
		 */
	}
	
}
