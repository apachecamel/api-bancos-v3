package com.accenture.spring.camel.api.processor.log;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.json.simple.JsonArray;
import org.apache.camel.json.simple.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.dto.Resultados;
import com.accenture.spring.camel.api.model.Cuenta;

@Component
public class LogearSingleton implements Processor {
	
	@Autowired
	private Resultados resultados;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		
		
		for(int i=0; i < resultados.getExitosos().size() ; i++) {
			
			System.out.println("-- exitoso numero -- "+i);
			System.out.println(resultados.getExitosos().get(i));
			
		}
		
		for(int i=0; i < resultados.getQueue().size() ; i++) {
			
			System.out.println("-- queue numero -- "+i);
			System.out.println(resultados.getQueue().get(i));
			
		}
		
		for(int i=0; i < resultados.getFallidos().size() ; i++) {
			
			System.out.println("-- fallido numero -- "+i);
			System.out.println(resultados.getFallidos().get(i));
			
		}
		
		System.out.println(" ");

		System.out.println("========== LOG ==========");
		
		System.out.println(" ");
		
		System.out.println("-------------- exitosos :"+resultados.getExitosos().size());
		
		for (Cuenta exi : resultados.getExitosos()) {
			
			System.out.println("--------");
			System.out.println(exi.getName() + " ID:" + exi.getId());
			System.out.println(exi.getSaldo());
			System.out.println(exi.getCategoria());
			
		}
		
		System.out.println("-------------- a queue :"+resultados.getQueue().size());
		
		for (Cuenta que : resultados.getQueue()) {
			
			System.out.println("--------");
			System.out.println(que.getName());
			System.out.println(que.getSaldo());
			System.out.println(que.getCategoria());
		}
		
		System.out.println("-------------- fallidos :"+resultados.getFallidos().size());
		
		for (Cuenta fail : resultados.getFallidos()) {
			
			System.out.println("--------");
			System.out.println(fail.getName());
			System.out.println(fail.getSaldo());
			System.out.println(fail.getCategoria());
		}	
		
		
		
		
		
//		Genera un JSON Array con todo lo necesario para mostrar, pero por ahora no sirve
		
		JsonObject rdos = new JsonObject();
			
		JsonArray exitosos = new JsonArray();
		
		JsonArray queue = new JsonArray();
		
		JsonArray fallidos = new JsonArray();
		
		//voy a tener un resultados.put de los 3 arrays
		
		for (int j=0; j < resultados.getExitosos().size() ; j++) {
			
			JsonObject aux = new JsonObject();
			
			aux.put("nombre", resultados.getExitosos().get(j).getName());
			aux.put("saldo", resultados.getExitosos().get(j).getSaldo());
			aux.put("categoria", resultados.getExitosos().get(j).getCategoria());
			
			exitosos.add(aux);
			
		}
		
		for (int j=0; j < resultados.getQueue().size() ; j++) {
			
			JsonObject aux = new JsonObject();
			
			aux.put("nombre", resultados.getQueue().get(j).getName());
			aux.put("saldo", resultados.getQueue().get(j).getSaldo());
			aux.put("categoria", resultados.getQueue().get(j).getCategoria());
			
			queue.add(aux);
		}
		
		for(int j=0; j < resultados.getFallidos().size() ; j++) {
			
			JsonObject aux = new JsonObject();
			
			aux.put("nombre", resultados.getFallidos().get(j).getName());
			aux.put("saldo", resultados.getFallidos().get(j).getSaldo());
			aux.put("categoria", resultados.getFallidos().get(j).getCategoria());
			
			fallidos.add(aux);
		}
		
		rdos.put("exitosos", exitosos);
		rdos.put("queue", queue);
		rdos.put("fallidos", fallidos);
		
		exchange.getOut().setBody(rdos); 
		
		//vacio el singleton para que en la proxima llamada no joda
		
		resultados.getExitosos().clear();
		resultados.getFallidos().clear();
		resultados.getQueue().clear();
		
	}
}
