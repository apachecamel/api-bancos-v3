package com.accenture.spring.camel.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath:Beans.xml")
public class CamelRestDslApplication {

	public static void main(String[] args) {
		SpringApplication.run(CamelRestDslApplication.class, args);
		
	}

}
