package com.accenture.spring.camel.api.processor.id;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.json.simple.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.dao.CuentaDao;
import com.accenture.spring.camel.api.exceptions.CuentaNotFoundException;

@Component
public class GetCuentaByIdProcessor implements Processor {

	@Autowired
	private CuentaDao cuentadao;

	@Override
	public void process(Exchange exchange) throws Exception {
		JsonObject j = new JsonObject();

			if (cuentadao.findById(Long.parseLong(exchange.getMessage().getHeader("id").toString()))
					.orElse(null) == null)
				throw (new CuentaNotFoundException());
			else {
				j.put("Cuenta", cuentadao.findById(Long.parseLong(exchange.getMessage().getHeader("id").toString()))
						.orElse(null));
				j.put("error", 0);
				exchange.getOut().setBody(j);
			}

	}

}
