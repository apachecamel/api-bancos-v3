package com.accenture.spring.camel.api.resource.rest;

import javax.print.Doc;

import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.constants.ErrorConstants;
import com.accenture.spring.camel.api.exceptions.CuentaNotFoundException;
import com.accenture.spring.camel.api.exceptions.MontoInvalidoException;
import com.accenture.spring.camel.api.exceptions.SaldoInsuficienteException;
import com.accenture.spring.camel.api.model.Cuenta;
import com.accenture.spring.camel.api.model.ErrorMessage;
import com.accenture.spring.camel.api.model.Transaccion;
import com.accenture.spring.camel.api.processor.id.DepositoProcessor;
import com.accenture.spring.camel.api.processor.id.ExtraccionProcessor;
import com.accenture.spring.camel.api.processor.id.TransaccionProcessor;
import com.accenture.spring.camel.api.resource.ErrorHandlerRoute;

/**
 * Contiene todas las rutas REST de tipo PUT.
 * 
 * @Authors santiago.ferreiro , ignacio.speicys
 *
 */
@Component
public class PutResource extends ErrorHandlerRoute {

	@BeanInject
	private DepositoProcessor procesadordepo;

	@BeanInject
	private ExtraccionProcessor procesadorextr;

	@BeanInject
	private TransaccionProcessor procesadortransaccion;

	@Override
	public void configure() throws Exception {
		super.configure();
		rest("/cuenta")
			//Update de Saldo en Cuenta
			.put("/saldo").consumes(MediaType.APPLICATION_JSON_VALUE).type(Cuenta.class)
				// SWAGGER
				.description(
						"Cambia el Saldo de la cuenta, en funcion del numero ingresado. usar n<0 para restar, n>0 para sumar")
				.responseMessage().code(200).responseModel(Cuenta.class).endResponseMessage() // OK
				.responseMessage().code(400).responseModel(ErrorMessage.class).endResponseMessage() // Not-OK
				.responseMessage().code(400).responseModel(ErrorMessage.class).endResponseMessage() // Not-OK
				// RUTA
				.route().doTry()
				.choice().when().simple("${body.saldo} >= 0")
					.process(procesadordepo)
				.otherwise()
					.process(procesadorextr)
				.endDoTry()
				.doCatch(CuentaNotFoundException.class)
					.to("direct:cuentaNotFound")
				.endDoTry()
				.doCatch(SaldoInsuficienteException.class)
					.to("direct:SaldoInsuficiente")
				
			.endRest()
			
			//Transaccion entre dos cuentas
			.put("/transaccion").consumes(MediaType.APPLICATION_JSON_VALUE).type(Transaccion.class)
				// SWAGGER
				.description("Acciona una transaccion dado un monto determinado y 2 IDs de cuentas existentes")
				.responseMessage().code(200).message("OK").responseModel(ErrorMessage.class).endResponseMessage()
				.responseMessage().code(404).message("USER NOT FOUND").responseModel(ErrorMessage.class).endResponseMessage()
				.responseMessage().code(406).message("SALDO INSUFICIENTE").responseModel(ErrorMessage.class).endResponseMessage()
				.responseMessage().code(406).message("TRANSACCION INVALIDA").responseModel(ErrorMessage.class).endResponseMessage()
				// RUTA
				.route()
				.doTry()
					.choice().when().simple("${body.monto} < 0")
											.throwException(new MontoInvalidoException())
							.otherwise().process(procesadortransaccion).log("${headers}")
							.choice()
									.when().simple("${header.CamelHttpResponseCode} == 200")
									.otherwise().marshal().json(JsonLibrary.Jackson)
							.end()
					.end()
				.endDoTry()
					.doCatch(CuentaNotFoundException.class)
						.to("direct:cuentaNotFound")			
					.endDoTry()
					.doCatch(SaldoInsuficienteException.class)
						.to("direct:SaldoInsuficiente")
					.endDoTry()
					.doCatch(MontoInvalidoException.class)
						.to("direct:transaccionInvalida")
			.endRest();

	}

}
