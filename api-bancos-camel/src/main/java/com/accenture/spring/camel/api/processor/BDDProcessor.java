package com.accenture.spring.camel.api.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.dao.CuentaDao;
import com.accenture.spring.camel.api.model.Cuenta;

/**
 * Procesador que guarda un POJO de tipo Cuenta.class en base de datos mediante Hibernate.
 * 
 * 
 * @author ignacio.speicys , santiago.ferreiro
 *
 */
@Component
public class BDDProcessor implements Processor {
	
	@Autowired
	private CuentaDao cuentadao;

	@Override
	public void process(Exchange exchange) throws Exception {
		
		cuentadao.save(exchange.getIn().getBody(Cuenta.class));
		
	}

}
